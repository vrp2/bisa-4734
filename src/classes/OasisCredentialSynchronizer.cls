public with sharing class OasisCredentialSynchronizer implements Schedulable {
    public static final String HTTP_DELETE                    = 'DELETE';
    public static final String HTTP_PUT                       = 'PUT';
    public static final String GROUP_DEVELOPER_NAME           = 'OASIS_Credentials_Responsible_persons';
    public static final String OASIS_UN                       = Label.OASIS_Marketing_Check_Username;
    public static final String OASIS_PW                       = Label.OASIS_Marketing_Check_Password;
//    public static final String END_POINT    = 'https://sec14community-developer-edition.ap24.force.com/services/apexrest/BISA';
    public static final String END_POINT    = 'https://team-defenders.ingress.kubernetes.staging.irl.aws.tipicodev.com//v1/oasis/auth';
//    public static final String END_POINT = 'https://api.staging.irl.aws.tipicodev.com/v1/oasis/auth';

    public DataWrapper wrapper;

    public void execute(SchedulableContext sc) {
        System.debug('OasisCredentialSynchronizer.execute()');
        System.debug(wrapper);
        if (wrapper != null) {
            sendCredentials(JSON.serialize(this.wrapper));
        }
    }

    @TestVisible
    private static void tryCalloutAgain(DataWrapper wrapper) {
        DateTime nextRunTime = DateTime.now().addSeconds(30);
        System.debug('#### nextRunTime is ' + nextRunTime);

        String cronString = '' + nextRunTime.second() + ' '   + nextRunTime.minute() + ' ' +
                                 nextRunTime.hour()   + ' '   + nextRunTime.day()    + ' ' +
                                 nextRunTime.month()  + ' ? ' + nextRunTime.year();

        OasisCredentialSynchronizer credSynchronizer = new OasisCredentialSynchronizer();
        credSynchronizer.wrapper                     = wrapper;

        System.schedule(OasisCredentialSynchronizer.class.getName() + '-' + nextRunTime.format(),
                        cronString,
                        credSynchronizer);
    }


    @Future(Callout = true)
    public static void sendCredentials(String wrapperJson) {
        System.debug('sendCredentials method');
        if (Limits.getCallouts() >= Limits.getLimitCallouts()) {
            String errorMessage = 'The OASIS Credential synchronization cannot be performed right now. Kindly contact the Salesforce Team for support.';
            NotifyAdminController.sendNotification('Oasis Marketing Check: Maximum number of callouts has been reached.', 'Maximum number of callouts has been reached in AccountOasisMarketingCheckHelper.cls.', null);
            // configure and perform the callout
        }
        DataWrapper wrapper = (DataWrapper) JSON.deserialize(wrapperJson, DataWrapper.class);

        HttpRequest request = new HttpRequest();
        request.setTimeout(30000);


//        request.setEndpoint('https://api.staging.irl.aws.tipicodev.com/v1/oasis/customer-status/227932ee-f59a-4421-a1d0-58a3dbb63624');
//        request.setMethod('GET');

//        request.setHeader('Content-Type', 'application/vnd.tipico.auth.channel.credentials.request-v1+json; charset=ISO-8859-1');
//        request.setHeader('Authorization', 'Basic c2FsZXNmb3JjZTpnZWhlaW0=');

        request.setEndpoint(END_POINT + '/' + wrapper.accept_Channel_Id);
        if (wrapper.httpMethod == HTTP_DELETE) {
            request.setMethod(HTTP_DELETE);
        } else if (wrapper.httpMethod == HTTP_PUT) {
            request.setMethod(HTTP_PUT);
            String data = '{"oasisUsername":"' + wrapper.username + '", "oasisPassword":"' + wrapper.password + '"}';
            request.setBody(data);
        }

        Http http = new Http();
        try {
            HttpResponse response = http.send(request);
            Integer      status   = response.getStatusCode();
            System.debug('--- status - ' + status);
            System.debug(response.getBody());
            System.debug('time: ' + System.now());

            if (status == 200) {
                if (wrapper.unSynchronizedOasisCredentials == true) {
                    setUnSynchronizedOasisCredsField(wrapper.confidentialId, false);
                }
            } else {
                setUnSynchronizedOasisCredsField(wrapper.confidentialId, true);
                handleError(wrapper, null, status);
            }
        } catch (Exception e) {
            wrapper.attemptNumber++;
            System.debug('Exception - ' + e);
            System.debug(' - attemptNumber - ' + wrapper.attemptNumber);
            if (wrapper.attemptNumber < 3) {
                tryCalloutAgain(wrapper);
            } else {
                setUnSynchronizedOasisCredsField(wrapper.confidentialId, true);
                handleError(wrapper, e.getMessage(), null);
            }
        }
    }

    @TestVisible
    private static void setUnSynchronizedOasisCredsField(String confidentialId, Boolean unSynchronizedOasisCredentials) {
        System.debug('updateUnSynchronizedOasisCredsField - ' + unSynchronizedOasisCredentials);
        Confidential__c confToUpdate = [SELECT id, Unsynchronized_OASIS_credentials__c FROM Confidential__c WHERE Id = :confidentialId LIMIT 1];
        confToUpdate.Unsynchronized_OASIS_credentials__c = unSynchronizedOasisCredentials;
        update confToUpdate;
    }

    @TestVisible
    private static void handleError(DataWrapper wrapper, String errorMessage, Integer status) {
        String errorString =  'An error occurred while synchronizing OASIS Credentials in "OasisCredentialSynchronizer.cls". \n';
        errorString        += 'Organization Id: '     + UserInfo.getOrganizationId() + ', \n';
        if(errorMessage != null) {
            errorString    += 'Error message: '       + errorMessage + ', \n';
        }
        if (status != null) {
            errorString    += 'HttpResponse Status: ' + status + ', \n';
        }
        errorString        += 'HttpMethod: '          + wrapper.httpMethod + ', \n';
        errorString        += 'Object - Confidential, confidentialId = '       + wrapper.confidentialId     + ', \n';
        errorString        += 'Ebet Accept Channel Id = '                      + wrapper.accept_Channel_Id  + '.\n';

        System.debug(errorString);

        insert new ExceptionLogger__c(errorText__c = errorString);
//        sendEmailToResponsiblePerson(errorString);
//        NotifyAdminController.sendNotification('OASIS Credentials synchronization notification: OasisCredentialSynchronizer', errorString, null);
    }

    @TestVisible
    private static void sendEmailToResponsiblePerson(String emailBody) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<GroupMember> gMembers = [
                SELECT Id, Group.DeveloperName, UserOrGroupId
                FROM GroupMember
                WHERE Group.DeveloperName = :GROUP_DEVELOPER_NAME
        ];
        List<Id> usersIds = new List<Id>();
        for (GroupMember groupMember: gMembers) {
            usersIds.add(groupMember.UserOrGroupId);
        }

        List<User> users = [SELECT Id, Email FROM User WHERE Id IN :usersIds];
        if(users.size() == 0) {
            return ;
        }

        List<String> userEmails = new List<String>();
        for (User us: users) {
            userEmails.add(us.Email);
        }
        mail.setToAddresses(userEmails);
        mail.setSubject('Oasis Credentials aren\'t synchronized.');
        mail.setHTMLBody(emailBody);

        OrgWideEmailAddress[] owea = [
                SELECT Id
                FROM OrgWideEmailAddress
                WHERE Address = 'salesforceadmin@tipico.com'
        ];
        if(owea.size() > 0) {
            mail.setOrgWideEmailAddressId(owea[0].Id);
        }

//        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }

    public class DataWrapper {
        public String   accept_Channel_Id;
        public String   confidentialId;
        public String   username;
        public String   password;
        public Integer  attemptNumber;
        public Boolean  unSynchronizedOasisCredentials;
        public String   httpMethod;
    }
}